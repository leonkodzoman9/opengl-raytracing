#version 460 core



out vec2 fragmentUV;



const vec2 vertices[4] = vec2[4](vec2(-1, -1), vec2(1, -1), vec2(1, 1), vec2(-1, 1));
const ivec2 uvs[4] = ivec2[4](ivec2(0, 0), ivec2(1, 0), ivec2(1, 1), ivec2(0, 1));



void main() {

    gl_Position = vec4(vertices[gl_VertexID], 0, 1);
    
    fragmentUV = uvs[gl_VertexID];
} 
