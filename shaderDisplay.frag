#version 460 core

layout (location = 0) out vec4 outputColor;



layout (binding = 0) uniform sampler2D tex;

uniform int framesStill;



in vec2 fragmentUV;



void main() {
    outputColor = vec4(texture(tex, fragmentUV).xyz / float(framesStill), 1);
}


