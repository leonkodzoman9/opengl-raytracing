#pragma once

#include "Includes.hpp"



namespace OGLRT {

	class BufferGL;

	using BufferDescriptorID = uint32_t;



	enum class BDType : uint32_t {
		FLOAT = GL_FLOAT,
		DOUBLE = GL_DOUBLE
	};
	enum class BDComponent : uint32_t {
		VEC1 = 1,
		VEC2 = 2,
		VEC3 = 3,
		VEC4 = 4,
	};



	struct BDSegmentLayout {

		uint32_t byteOffset;
		uint32_t byteStride;
		BDComponent component;
		BDType type;
		BufferGL* buffer;
	};



	class BufferDescriptorGL {

	private:

		BufferDescriptorID ID = 0;

	public:

		BufferDescriptorGL();
		~BufferDescriptorGL();

		BufferDescriptorGL(const BufferDescriptorGL& rhs) = delete;
		BufferDescriptorGL& operator=(const BufferDescriptorGL& rhs) = delete;
		BufferDescriptorGL(BufferDescriptorGL&& rhs) noexcept = delete;
		BufferDescriptorGL& operator=(BufferDescriptorGL&& descriptor) noexcept;

		const BufferDescriptorID getID() const;

		void use() const;

		void setLayout(std::initializer_list<BDSegmentLayout> segments, const BufferGL& indexBuffer);
	};



}
