#include "Keyboard.hpp"

#include "Window.hpp"



uint32_t getKeyValue(OGLRT::Key key) {

	switch (key) {
	case OGLRT::Key::A:				return GLFW_KEY_A;
	case OGLRT::Key::B:				return GLFW_KEY_B;
	case OGLRT::Key::C:				return GLFW_KEY_C;
	case OGLRT::Key::D:				return GLFW_KEY_D;
	case OGLRT::Key::E:				return GLFW_KEY_E;
	case OGLRT::Key::F:				return GLFW_KEY_F;
	case OGLRT::Key::G:				return GLFW_KEY_G;
	case OGLRT::Key::H:				return GLFW_KEY_H;
	case OGLRT::Key::I:				return GLFW_KEY_I;
	case OGLRT::Key::J:				return GLFW_KEY_J;
	case OGLRT::Key::K:				return GLFW_KEY_K;
	case OGLRT::Key::L:				return GLFW_KEY_L;
	case OGLRT::Key::M:				return GLFW_KEY_M;
	case OGLRT::Key::N:				return GLFW_KEY_N;
	case OGLRT::Key::O:				return GLFW_KEY_O;
	case OGLRT::Key::P:				return GLFW_KEY_P;
	case OGLRT::Key::Q:				return GLFW_KEY_Q;
	case OGLRT::Key::R:				return GLFW_KEY_R;
	case OGLRT::Key::S:				return GLFW_KEY_S;
	case OGLRT::Key::T:				return GLFW_KEY_T;
	case OGLRT::Key::U:				return GLFW_KEY_U;
	case OGLRT::Key::V:				return GLFW_KEY_V;
	case OGLRT::Key::W:				return GLFW_KEY_W;
	case OGLRT::Key::X:				return GLFW_KEY_X;
	case OGLRT::Key::Y:				return GLFW_KEY_Y;
	case OGLRT::Key::Z:				return GLFW_KEY_Z;
	case OGLRT::Key::ALPHA_0:		return GLFW_KEY_0;
	case OGLRT::Key::ALPHA_1:		return GLFW_KEY_1;
	case OGLRT::Key::ALPHA_2:		return GLFW_KEY_2;
	case OGLRT::Key::ALPHA_3:		return GLFW_KEY_3;
	case OGLRT::Key::ALPHA_4:		return GLFW_KEY_4;
	case OGLRT::Key::ALPHA_5:		return GLFW_KEY_5;
	case OGLRT::Key::ALPHA_6:		return GLFW_KEY_6;
	case OGLRT::Key::ALPHA_7:		return GLFW_KEY_7;
	case OGLRT::Key::ALPHA_8:		return GLFW_KEY_8;
	case OGLRT::Key::ALPHA_9:		return GLFW_KEY_9;
	case OGLRT::Key::LEFT_SHIFT:		return GLFW_KEY_LEFT_SHIFT;
	case OGLRT::Key::LEFT_CONTROL:	return GLFW_KEY_LEFT_CONTROL;
	case OGLRT::Key::LEFT_ALT:		return GLFW_KEY_LEFT_ALT;
	case OGLRT::Key::RIGHT_SHIFT:	return GLFW_KEY_RIGHT_SHIFT;
	case OGLRT::Key::RIGHT_CONTROL:	return GLFW_KEY_RIGHT_CONTROL;
	case OGLRT::Key::RIGHT_ALT:		return GLFW_KEY_RIGHT_ALT;
	case OGLRT::Key::SPACE:			return GLFW_KEY_SPACE;
	case OGLRT::Key::COMMA:			return GLFW_KEY_COMMA;
	case OGLRT::Key::MINUS:			return GLFW_KEY_MINUS;
	case OGLRT::Key::PERIOD:			return GLFW_KEY_PERIOD;
	case OGLRT::Key::SLASH:			return GLFW_KEY_SLASH;
	case OGLRT::Key::SEMICOLON:		return GLFW_KEY_SEMICOLON;
	case OGLRT::Key::EQUAL:			return GLFW_KEY_EQUAL;
	case OGLRT::Key::ESCAPE:			return GLFW_KEY_ESCAPE;
	case OGLRT::Key::ENTER:			return GLFW_KEY_ENTER;
	case OGLRT::Key::TAB:			return GLFW_KEY_TAB;
	case OGLRT::Key::BACKSPACE:		return GLFW_KEY_BACKSPACE;
	case OGLRT::Key::RIGHT:			return GLFW_KEY_RIGHT;
	case OGLRT::Key::LEFT:			return GLFW_KEY_LEFT;
	case OGLRT::Key::UP:				return GLFW_KEY_UP;
	case OGLRT::Key::DOWN:			return GLFW_KEY_DOWN;
	case OGLRT::Key::KP_0:			return GLFW_KEY_KP_0;
	case OGLRT::Key::KP_1:			return GLFW_KEY_KP_1;
	case OGLRT::Key::KP_2:			return GLFW_KEY_KP_2;
	case OGLRT::Key::KP_3:			return GLFW_KEY_KP_3;
	case OGLRT::Key::KP_4:			return GLFW_KEY_KP_4;
	case OGLRT::Key::KP_5:			return GLFW_KEY_KP_5;
	case OGLRT::Key::KP_6:			return GLFW_KEY_KP_6;
	case OGLRT::Key::KP_7:			return GLFW_KEY_KP_7;
	case OGLRT::Key::KP_8:			return GLFW_KEY_KP_8;
	case OGLRT::Key::KP_9:			return GLFW_KEY_KP_9;
	case OGLRT::Key::KP_DIVIDE:		return GLFW_KEY_KP_DIVIDE;
	case OGLRT::Key::KP_MULTIPLY:	return GLFW_KEY_KP_MULTIPLY;
	case OGLRT::Key::KP_ADD:			return GLFW_KEY_KP_ADD;
	case OGLRT::Key::KP_SUBTRACT:	return GLFW_KEY_KP_SUBTRACT;
	}

	return 0;
}



namespace OGLRT {

	Keyboard& Keyboard::getInstance() {

		static Keyboard instance;

		return instance;
	}

	void Keyboard::update() {

		GLFWwindow* window = Window::getInstance().getWindow();

		this->currentTime = std::chrono::steady_clock::now();

		for (int i = 0; i < this->currentState.size(); i++) {

			Key key = Key(i);

			this->previousState[i] = this->currentState[i];
			this->currentState[i] = glfwGetKey(window, getKeyValue(key)) == GLFW_PRESS;

			bool current = this->currentState[i];
			bool previous = this->previousState[i];

			if (previous == false && current == true) {
				this->updatePressTime(key);
			}

			if (previous == true && current == false) {
				this->updateReleaseTime(key);
			}
		}
	}

	bool Keyboard::held(Key key) const {

		return this->currentState[(int)key];
	}
	bool Keyboard::pressed(Key key) const {

		return this->currentState[(int)key] && !this->previousState[(int)key];
	}
	bool Keyboard::released(Key key) const {

		return !this->currentState[(int)key] && this->previousState[(int)key];
	}

	double Keyboard::heldFor(Key key) const {

		if (this->held(key)) {
			return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->pressTimes[(uint32_t)key].current).count() / 1000000.0;
		}

		return 0;
	}
	double Keyboard::releasedFor(Key key) const {

		if (!this->held(key)) {
			return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->releaseTimes[(uint32_t)key].current).count() / 1000000.0;
		}

		return 0;
	}

	double Keyboard::pressDelta(Key key) const {

		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->pressTimes[(uint32_t)key].current - this->pressTimes[(uint32_t)key].previous).count() / 1000000.0;
	}
	double Keyboard::releaseDelta(Key key) const {

		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->releaseTimes[(uint32_t)key].current - this->releaseTimes[(uint32_t)key].previous).count() / 1000000.0;
	}

	

	void Keyboard::updatePressTime(Key key) {

		this->pressTimes[(uint32_t)key].previous = this->pressTimes[(uint32_t)key].current;
		this->pressTimes[(uint32_t)key].current = this->currentTime;
	}
	void Keyboard::updateReleaseTime(Key key) {

		this->releaseTimes[(uint32_t)key].previous = this->releaseTimes[(uint32_t)key].current;
		this->releaseTimes[(uint32_t)key].current = this->currentTime;
	}
}
