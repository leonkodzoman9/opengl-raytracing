#include "BufferDescriptorGL.hpp"

#include "BufferGL.hpp"



namespace OGLRT {

    BufferDescriptorGL::BufferDescriptorGL() {
        glCreateVertexArrays(1, &this->ID);
    }
    BufferDescriptorGL::~BufferDescriptorGL() {
        glDeleteVertexArrays(1, &this->ID);
    }

    BufferDescriptorGL& BufferDescriptorGL::operator = (BufferDescriptorGL&& descriptor) noexcept {

        if (this != &descriptor) {
            glDeleteVertexArrays(1, &this->ID);
            this->ID = std::exchange(descriptor.ID, 0);
        }

        return *this;
    }

    const BufferDescriptorID BufferDescriptorGL::getID() const {
        return this->ID;
    }

    void BufferDescriptorGL::use() const {
        glBindVertexArray(this->ID);
    }

    void BufferDescriptorGL::setLayout(std::initializer_list<BDSegmentLayout> segments, const BufferGL& indexBuffer) {

        int index = 0;
        for (const BDSegmentLayout& segment : segments) {

            glVertexArrayVertexBuffer(this->ID, index, segment.buffer->getID(), 0, segment.byteStride);
            if (segment.type == BDType::DOUBLE) {
                glVertexArrayAttribLFormat(this->ID, index, (uint32_t)segment.component, (uint32_t)segment.type, segment.byteOffset);
            }
            else {
                glVertexArrayAttribFormat(this->ID, index, (uint32_t)segment.component, (uint32_t)segment.type, GL_FALSE, segment.byteOffset);
            }
            glVertexArrayAttribBinding(this->ID, index, index);
            glEnableVertexArrayAttrib(this->ID, index);

            index++;
        }

        glVertexArrayElementBuffer(this->ID, indexBuffer.getID());
    }
}

