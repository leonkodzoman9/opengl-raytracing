#include "TextureParameter.hpp"



namespace OGLRT {

	uint32_t getTextureParameterNameGLType(TextureParameterName name) {

		switch (name) {
		case TextureParameterName::MIN_FILTER:	return GL_TEXTURE_MIN_FILTER;
		case TextureParameterName::MAG_FILTER:	return GL_TEXTURE_MAG_FILTER;
		case TextureParameterName::WRAP_S:		return GL_TEXTURE_WRAP_S;
		case TextureParameterName::WRAP_T:		return GL_TEXTURE_WRAP_T;
		case TextureParameterName::WRAP_R:		return GL_TEXTURE_WRAP_R;
		}

		return 0;
	}
	uint32_t getTextureParameterValueGLType(TextureParameterValue value) {

		switch (value) {
		case TextureParameterValue::NEAREST:				return GL_NEAREST;
		case TextureParameterValue::LINEAR:					return GL_LINEAR;
		case TextureParameterValue::NEAREST_MIPMAP_NEAREST:	return GL_NEAREST_MIPMAP_NEAREST;
		case TextureParameterValue::LINEAR_MIPMAP_NEAREST:	return GL_LINEAR_MIPMAP_NEAREST;
		case TextureParameterValue::NEAREST_MIPMAP_LINEAR:	return GL_NEAREST_MIPMAP_LINEAR;
		case TextureParameterValue::LINEAR_MIPMAP_LINEAR:	return GL_LINEAR_MIPMAP_LINEAR;
		case TextureParameterValue::CLAMP_TO_EDGE:			return GL_CLAMP_TO_EDGE;
		case TextureParameterValue::CLAMP_TO_BORDER:		return GL_CLAMP_TO_BORDER;
		case TextureParameterValue::MIRRORED_REPEAT:		return GL_MIRRORED_REPEAT;
		case TextureParameterValue::REPEAT:					return GL_REPEAT;
		case TextureParameterValue::MIRROR_CLAMP_TO_EDGE:	return GL_MIRROR_CLAMP_TO_EDGE;
		}

		return 0;
	}
}