#pragma once

#include "Includes.hpp"



namespace OGLRT {
    


    class Timer {

    protected:

        std::chrono::steady_clock::time_point start;
        std::chrono::steady_clock::time_point stop;

    public:

        Timer();

        void tickStart();
        void tickStop();
        void update();

        double getInterval() const;
    };



    class MultiTimer {

    private:

        Timer timer;

        std::vector<double> intervals;
        int currentIndex;

    public:

        MultiTimer(int intervalCount = 20);

        void tickStart();
        void tickStop();
        void update();

        double getAverageInterval() const;
    };
}
