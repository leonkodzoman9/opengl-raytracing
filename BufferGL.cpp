#include "BufferGL.hpp"

#include "Includes.hpp"



namespace OGLRT {

	BufferGL::BufferGL() {
		glCreateBuffers(1, &this->ID);
	}
	BufferGL::~BufferGL() {
		glDeleteBuffers(1, &this->ID);
	}

	BufferGL& BufferGL::operator = (BufferGL&& buffer) noexcept {

		if (this != &buffer) {
			std::swap(this->ID, buffer.ID);
		}

		return *this;
	}

	const BufferID BufferGL::getID() const {
		return this->ID;
	}

	void BufferGL::allocateStorage(void* data, uint32_t bytes, BufferFlags flags) {
		glNamedBufferStorage(this->ID, bytes, data, (uint32_t)flags);
	}
	void BufferGL::updateData(void* data, uint32_t bytes, uint32_t bytesOffset) {
		glNamedBufferSubData(this->ID, bytesOffset, bytes, data);
	}

	void BufferGL::bindToIndex(uint32_t index, IndexType bufferIndex) const {
		glBindBufferBase((uint32_t)bufferIndex, index, this->ID);
	}
}
