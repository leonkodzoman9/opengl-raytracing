#pragma once

#include "Includes.hpp"



namespace OGLRT {

	using BufferID = uint32_t;



	enum class BufferFlags : uint32_t {
		DYNAMIC_STORAGE = GL_DYNAMIC_STORAGE_BIT
	};

	enum class IndexType : uint32_t {
		UNIFORM_STORAGE = GL_UNIFORM_BUFFER,
		SHADER_STORAGE = GL_SHADER_STORAGE_BUFFER
	};



	class BufferGL {

	private:

		BufferID ID = 0;

	public:

		BufferGL();
		~BufferGL();
		
		BufferGL(const BufferGL& buffer) = delete;
		BufferGL& operator=(const BufferGL& buffer) = delete;
		BufferGL(BufferGL&& buffer) noexcept = delete;
		BufferGL& operator=(BufferGL&& buffer) noexcept;

		const BufferID getID() const;

		void allocateStorage(void* data, uint32_t bytes, BufferFlags flags = BufferFlags::DYNAMIC_STORAGE);
		void updateData(void* data, uint32_t bytes, uint32_t bytesOffset);

		void bindToIndex(uint32_t index, IndexType bufferIndex) const;
	};
}

