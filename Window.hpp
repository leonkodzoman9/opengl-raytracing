#pragma once

#include "Includes.hpp"



namespace OGLRT {

	enum class CursorMode {
		NORMAL = GLFW_CURSOR_NORMAL,
		HIDDEN = GLFW_CURSOR_HIDDEN,
		DISABLED = GLFW_CURSOR_DISABLED
	};



	class Window {

	private:

		glm::ivec2 size;
		std::string title;

		GLFWwindow* window;

	public:

		static Window& getInstance();

		void initialize(glm::ivec2 windowSize, std::string_view windowName = "Window");

		GLFWwindow* getWindow();

		glm::ivec2 getSize() const;
		std::string getTitle() const;
		double getAspectRatio() const;
		void setTitle(std::string_view windowName);

		void setCursorMode(CursorMode mode);


	private:

		Window();
		~Window();
	};
}

