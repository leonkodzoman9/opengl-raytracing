#include "Timer.hpp"



namespace OGLRT {

    Timer::Timer() {

        this->start = std::chrono::steady_clock::now();
        this->stop = std::chrono::steady_clock::now();
    }

    void Timer::tickStart() {

        this->start = std::chrono::steady_clock::now();
    }
    void Timer::tickStop() {

        this->stop = std::chrono::steady_clock::now();
    }
    void Timer::update() {

        this->start = this->stop;
        this->tickStop();
    }

    double Timer::getInterval() const {

        return std::chrono::duration_cast<std::chrono::nanoseconds>(this->stop - this->start).count() / 1000000.0;
    }



    MultiTimer::MultiTimer(int intervalCount) {

        this->currentIndex = 0;
        this->intervals = std::vector<double>(intervalCount);
    }

    void MultiTimer::tickStart() {

        this->timer.tickStart();
    }
    void MultiTimer::tickStop() {

        this->timer.tickStop();
    }
    void MultiTimer::update() {

        this->timer.update();

        this->intervals[this->currentIndex] = this->timer.getInterval();
        this->currentIndex = (this->currentIndex + 1) % this->intervals.size();
    }

    double MultiTimer::getAverageInterval() const {

        return std::accumulate(this->intervals.begin(), this->intervals.end(), 0.0) / this->intervals.size();
    }




}
