#include "Mouse.hpp"

#include "Window.hpp"



uint32_t getButtonValue(OGLRT::Button button) {

	switch (button) {
	case OGLRT::Button::BUTTON_MOUSE_LEFT:	return GLFW_MOUSE_BUTTON_LEFT;
	case OGLRT::Button::BUTTON_MOUSE_RIGHT:	return GLFW_MOUSE_BUTTON_RIGHT;
	case OGLRT::Button::BUTTON_MOUSE_MIDDLE:	return GLFW_MOUSE_BUTTON_MIDDLE;
	}

	return 0;
}



namespace OGLRT {

	double globalYOffset = 0;

#pragma warning(disable:4100)
	void scrollCallback(GLFWwindow* window, double xOffset, double yOffset) {
		globalYOffset = yOffset;
	}
#pragma warning(default:4100)



	Mouse& Mouse::getInstance() {

		static Mouse instance;

		return instance;
	}

	void Mouse::update() {

		GLFWwindow* window = Window::getInstance().getWindow();

		this->currentTime = std::chrono::steady_clock::now();

		this->scrollAmount = std::exchange(globalYOffset, 0);

		for (int i = 0; i < this->currentState.size(); i++) {

			Button button = Button(i);

			this->previousState[i] = this->currentState[i];
			this->currentState[i] = glfwGetMouseButton(window, getButtonValue(button)) == GLFW_PRESS;

			bool current = this->currentState[i];
			bool previous = this->previousState[i];

			if (previous == false && current == true) {
				this->updatePressTime(button);
			}

			if (previous == true && current == false) {
				this->updateReleaseTime(button);
			}
		}

		this->previousPosition = this->currentPosition;
		glfwGetCursorPos(window, &this->currentPosition.x, &this->currentPosition.y);
	}

	glm::dvec2 Mouse::position() const {

		return this->currentPosition;
	}
	glm::dvec2 Mouse::deltaPosition() const {

		return this->currentPosition - this->previousPosition;
	}

	double Mouse::scroll() const {

		return this->scrollAmount;
	}

	bool Mouse::held(Button button) const {

		return this->currentState[(uint32_t)button];
	}
	bool Mouse::pressed(Button button) const {

		return this->currentState[(uint32_t)button] == true && this->previousState[(uint32_t)button] == false;
	}
	bool Mouse::released(Button button) const {

		return this->currentState[(uint32_t)button] == false && this->previousState[(uint32_t)button] == true;
	}

	double Mouse::heldFor(Button button) const {

		if (this->held(button)) {
			return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->pressTimes[(uint32_t)button].current).count() / 1000000.0;
		}

		return 0;
	}
	double Mouse::releasedFor(Button button) const {

		if (!this->held(button)) {
			return std::chrono::duration_cast<std::chrono::nanoseconds>(this->currentTime - this->releaseTimes[(uint32_t)button].current).count() / 1000000.0;
		}

		return 0;
	}

	double Mouse::pressDelta(Button button) const {

		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->pressTimes[(uint32_t)button].current - this->pressTimes[(uint32_t)button].previous).count() / 1000000.0;
	}
	double Mouse::releaseDelta(Button button) const {

		return std::chrono::duration_cast<std::chrono::nanoseconds>(this->releaseTimes[(uint32_t)button].current - this->releaseTimes[(uint32_t)button].previous).count() / 1000000.0;
	}



	Mouse::Mouse() {

		glfwSetScrollCallback(Window::getInstance().getWindow(), scrollCallback);
	};

	void Mouse::updatePressTime(Button button) {

		this->pressTimes[(uint32_t)button].previous = this->pressTimes[(uint32_t)button].current;
		this->pressTimes[(uint32_t)button].current = this->currentTime;
	}
	void Mouse::updateReleaseTime(Button button) {

		this->releaseTimes[(uint32_t)button].previous = this->releaseTimes[(uint32_t)button].current;
		this->releaseTimes[(uint32_t)button].current = this->currentTime;
	}
}