#include "Window.hpp"



namespace OGLRT {

    Window& Window::getInstance() {

        static Window instance;

        return instance;
    }

    void Window::initialize(glm::ivec2 windowSize, std::string_view windowName) {

        this->size = windowSize;
        this->title = windowName;

        glfwInit();
        glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
        glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 6);
        glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

        this->window = glfwCreateWindow(windowSize.x, windowSize.y, windowName.data(), nullptr, nullptr);
        glfwMakeContextCurrent(this->window);

        gladLoadGLLoader((GLADloadproc)glfwGetProcAddress);
    }

    GLFWwindow* Window::getWindow() {

        return this->window;
    }

    glm::ivec2 Window::getSize() const {

        return this->size;
    }
    std::string Window::getTitle() const {

        return this->title;
    }
    double Window::getAspectRatio() const {
        return (double)this->size.x / (double)this->size.y;
    }
    void Window::setTitle(std::string_view windowName) {

        this->title = windowName;

        glfwSetWindowTitle(this->window, windowName.data());
    }

    void Window::setCursorMode(CursorMode mode) {

        glfwSetInputMode(Window::getInstance().getWindow(), GLFW_CURSOR, (uint32_t)mode);
    }



    Window::Window() {

        this->title = "Window";
        this->window = nullptr;
    }
    Window::~Window() {

        glfwTerminate();
    }
}