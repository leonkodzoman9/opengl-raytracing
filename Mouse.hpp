#pragma once

#include "Includes.hpp"



namespace OGLRT {

	enum class Button : uint32_t {

		BUTTON_MOUSE_LEFT,
		BUTTON_MOUSE_RIGHT,
		BUTTON_MOUSE_MIDDLE,

		BUTTON_COUNT
	};

	void scrollCallback(GLFWwindow* window, double xOffset, double yOffset);



	class Mouse {

	private:

		struct TimingPair {
			std::chrono::steady_clock::time_point previous;
			std::chrono::steady_clock::time_point current;
		};

	private:

		double scrollAmount = 0;

		std::array<bool, (uint32_t)Button::BUTTON_COUNT> currentState{};
		std::array<bool, (uint32_t)Button::BUTTON_COUNT> previousState{};

		std::array<TimingPair, (uint32_t)Button::BUTTON_COUNT> pressTimes{};
		std::array<TimingPair, (uint32_t)Button::BUTTON_COUNT> releaseTimes{};

		std::chrono::steady_clock::time_point currentTime{};

		glm::dvec2 previousPosition{};
		glm::dvec2 currentPosition{};

	public:

		static Mouse& getInstance();

		void update();

		glm::dvec2 position() const;
		glm::dvec2 deltaPosition() const;

		double scroll() const;

		bool held(Button button) const;
		bool pressed(Button button) const;
		bool released(Button button) const;

		double heldFor(Button button) const;
		double releasedFor(Button button) const;

		double pressDelta(Button button) const;
		double releaseDelta(Button button) const;

	private:

		Mouse();

		void updatePressTime(Button button);
		void updateReleaseTime(Button button);
	};

}



