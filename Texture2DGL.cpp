#include "Texture2DGL.hpp"



namespace OGLRT {

	Texture2DGL::Texture2DGL() {

		glCreateTextures(GL_TEXTURE_2D, 1, &this->ID);
	}
	Texture2DGL& Texture2DGL::operator=(Texture2DGL&& texture) noexcept {

		if (this != &texture) {
			glDeleteTextures(1, &this->ID);
			this->ID = std::exchange(texture.ID, 0);
		}

		return *this;
	}
	Texture2DGL::~Texture2DGL() {

		glDeleteTextures(1, &this->ID);
	}

	void Texture2DGL::allocateStorage(glm::ivec2 size, TexturePixelFormat format, int mipCount) {
		
		glTextureStorage2D(this->ID, mipCount, (uint32_t)format, size.x, size.y);
	}
	void Texture2DGL::updateData(void* data, glm::ivec2 offset, glm::ivec2 size, DataFormat format, DataType type, int mipmap) {

		glTextureSubImage2D(this->ID, mipmap, offset.x, offset.y, size.x, size.y, (uint32_t)format, (uint32_t)type, data);
	}

	void Texture2DGL::setParameter(TextureParameterName name, TextureParameterValue value) {

		glTextureParameteri(this->ID, (uint32_t)name, (uint32_t)value);
	}

	void Texture2DGL::generateMipmaps() {
		glGenerateTextureMipmap(this->ID);
	}

	const uint32_t Texture2DGL::getID() const {
		return this->ID;
	}

}
