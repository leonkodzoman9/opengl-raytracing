#pragma once

#include "Includes.hpp"



namespace OGLRT {

	enum class Key : uint32_t {

		A,
		B,
		C,
		D,
		E,
		F,
		G,
		H,
		I,
		J,
		K,
		L,
		M,
		N,
		O,
		P,
		Q,
		R,
		S,
		T,
		U,
		V,
		W,
		X,
		Y,
		Z,

		ALPHA_0,
		ALPHA_1,
		ALPHA_2,
		ALPHA_3,
		ALPHA_4,
		ALPHA_5,
		ALPHA_6,
		ALPHA_7,
		ALPHA_8,
		ALPHA_9,

		LEFT_SHIFT,
		LEFT_CONTROL,
		LEFT_ALT,

		RIGHT_SHIFT,
		RIGHT_CONTROL,
		RIGHT_ALT,

		SPACE,
		COMMA,
		MINUS,
		PERIOD,
		SLASH,
		SEMICOLON,
		EQUAL,
		ESCAPE,
		ENTER,
		TAB,
		BACKSPACE,
		RIGHT,
		LEFT,
		UP,
		DOWN,

		KP_0,
		KP_1,
		KP_2,
		KP_3,
		KP_4,
		KP_5,
		KP_6,
		KP_7,
		KP_8,
		KP_9,

		KP_DIVIDE,
		KP_MULTIPLY,
		KP_ADD,
		KP_SUBTRACT,

		KEY_COUNT = KP_SUBTRACT - A + 1
	};



	class Keyboard {

	private:

		struct TimingPair {
			std::chrono::steady_clock::time_point previous;
			std::chrono::steady_clock::time_point current;
		};

	private:

		std::array<bool, (uint32_t)Key::KEY_COUNT> currentState{};
		std::array<bool, (uint32_t)Key::KEY_COUNT> previousState{};

		std::array<TimingPair, (uint32_t)Key::KEY_COUNT> pressTimes{};
		std::array<TimingPair, (uint32_t)Key::KEY_COUNT> releaseTimes{};

		std::chrono::steady_clock::time_point currentTime{};

	public:

		static Keyboard& getInstance();

		void update();

		bool held(Key key) const;
		bool pressed(Key key) const;
		bool released(Key key) const;

		double heldFor(Key key) const;
		double releasedFor(Key key) const;

		double pressDelta(Key key) const;
		double releaseDelta(Key key) const;

	private:

		Keyboard() = default;

		void updatePressTime(Key key);
		void updateReleaseTime(Key key);
	};



}
