#pragma once

#include "Includes.hpp"

#include "PixelFormat.hpp"
#include "TextureParameter.hpp"



namespace OGLRT {

    using Texture2DID = uint32_t;



    class Texture2DGL {

    private:

        Texture2DID ID = 0;

    public:

        Texture2DGL();
        ~Texture2DGL();

        Texture2DGL(const Texture2DGL& rhs) = delete;
        Texture2DGL& operator=(const Texture2DGL& rhs) = delete;
        Texture2DGL(Texture2DGL&& rhs) noexcept = delete;
        Texture2DGL& operator=(Texture2DGL&& texture) noexcept;

        void allocateStorage(glm::ivec2 size, TexturePixelFormat format, int mipCount = 1);
        void updateData(void* data, glm::ivec2 offset, glm::ivec2 size, DataFormat format, DataType type, int mipmap);
        void setParameter(TextureParameterName name, TextureParameterValue value);

        void generateMipmaps();

        const uint32_t getID() const;
    };



}

