#include "Includes.hpp"

#include "Window.hpp"
#include "Mouse.hpp"
#include "Keyboard.hpp"
#include "BufferGL.hpp"
#include "BufferDescriptorGL.hpp"
#include "Texture2DGL.hpp"
#include "TextureParameter.hpp"
#include "PixelFormat.hpp"
#include "ShaderGL.hpp"
#include "Timer.hpp"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

using namespace OGLRT;



struct Camera {
	
	float yaw = -90;
	float pitch = 0;

	glm::vec3 position;
	glm::vec3 front;
	glm::vec3 up;
	glm::vec3 right;
	float fov;
	float focalLength;
};



struct Material {
	glm::vec4 diffuse;
	glm::vec4 emissive;
	glm::vec4 specular;

	Material(glm::vec4 diffuse, glm::vec4 emissive, glm::vec4 specular) : diffuse(diffuse), emissive(emissive), specular(specular) {}
};

struct Sphere {
	glm::vec4 posRad;
	glm::vec4 mins;
	glm::vec4 maxs;
	Material material;
	
	Sphere(glm::vec4 posRad, Material material) : posRad(posRad), material(material) {
		this->mins = glm::vec4(glm::vec3(posRad) - posRad.w, 0);
		this->maxs = glm::vec4(glm::vec3(posRad) + posRad.w, 0);
	}
};

struct Triangle {
	glm::vec4 p1;
	glm::vec4 p2;
	glm::vec4 p3;
	glm::vec4 mins;
	glm::vec4 maxs;
	Material material;

	Triangle(glm::vec4 p1, glm::vec4 p2, glm::vec4 p3, Material material) : p1(p1), p2(p2), p3(p3), material(material) {
		this->mins = glm::min(glm::min(p1, p2), p3);
		this->maxs = glm::max(glm::max(p1, p2), p3);
	}
};




void addQuad(glm::vec3 p1, glm::vec3 p2, glm::vec3 p3, glm::vec3 p4, Material material, std::vector<Triangle>& triangles) {
	triangles.push_back(Triangle(glm::vec4(p1, 0), glm::vec4(p2, 0), glm::vec4(p3, 0), material));
	triangles.push_back(Triangle(glm::vec4(p1, 0), glm::vec4(p3, 0), glm::vec4(p4, 0), material));
}

void addCube(glm::vec3 center, glm::vec3 scale, glm::quat rotation, Material mat, std::vector<Triangle>& tris, bool flip = false) {

	std::array<glm::vec3, 8> points;

	for (int i = 0; i < 2; i++) {
		for (int j = 0; j < 2; j++) {
			for (int k = 0; k < 2; k++) {

				glm::mat4 translate = glm::translate(center);
				glm::mat4 scalemat = glm::scale(scale);
				glm::mat4 rotmat = glm::mat4_cast(rotation);

				glm::vec3 point = translate * rotmat * scalemat * glm::vec4(i - 0.5, j - 0.5, k - 0.5, 1);

				points[4 * i + 2 * j + k] = point;
			}
		}
	}

	if (flip) {
		addQuad(points[0], points[2], points[3], points[1], mat, tris);
		addQuad(points[4], points[5], points[7], points[6], mat, tris);
		addQuad(points[1], points[3], points[7], points[5], mat, tris);
		addQuad(points[3], points[2], points[6], points[7], mat, tris);
		addQuad(points[2], points[0], points[4], points[6], mat, tris);
		addQuad(points[0], points[1], points[5], points[4], mat, tris);
	}
	else {
		addQuad(points[0], points[1], points[3], points[2], mat, tris);
		addQuad(points[4], points[6], points[7], points[5], mat, tris);
		addQuad(points[1], points[5], points[7], points[3], mat, tris);
		addQuad(points[3], points[7], points[6], points[2], mat, tris);
		addQuad(points[2], points[6], points[4], points[0], mat, tris);
		addQuad(points[0], points[4], points[5], points[1], mat, tris);
	}
}



int main() {

	Window& window = Window::getInstance();
	Mouse& mouse = Mouse::getInstance();
	Keyboard& keyboard = Keyboard::getInstance();

	glm::ivec2 wSize = glm::ivec2(1600, 900);
	window.initialize(wSize);
	window.setCursorMode(CursorMode::NORMAL);
	glfwSwapInterval(0);



	BufferDescriptorGL empty;
	Texture2DGL texture;
	texture.allocateStorage(wSize, TexturePixelFormat::RGBA32F);
	texture.setParameter(TextureParameterName::MIN_FILTER, TextureParameterValue::LINEAR);

	std::vector<Sphere> spheres;

	spheres.push_back(Sphere(glm::vec4(0.2, -0.8, 0.1, 0.2), Material(glm::vec4(1, 1, 1, 0.5), glm::vec4(0), glm::vec4(0))));

	spheres.push_back(Sphere(glm::vec4(-0.5, -1 + 0.1, 0.5, 0.1), Material(glm::vec4(1, 1, 1, 1), glm::vec4(0), glm::vec4(1, 1, 1, 0.8))));
	spheres.push_back(Sphere(glm::vec4(-0.5, -1 + 0.1, 0.5, 0.1), Material(glm::vec4(1, 1, 1, 1), glm::vec4(0), glm::vec4(1, 1, 1, 0.8))));

	spheres.push_back(Sphere(glm::vec4(0, -1 + 0.15, 0.7, 0.15), Material(glm::vec4(1, 1, 1, 1), glm::vec4(0), glm::vec4(1, 1, 0.1, 0.8))));

	std::vector<Triangle> triangles;

	addQuad(glm::vec3(-1, -1, 1), glm::vec3(1, -1, 1), glm::vec3(1, -1, -1), glm::vec3(-1, -1, -1), Material(glm::vec4(1, 1, 1, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addQuad(glm::vec3(-1, 1, 1), glm::vec3(-1, 1, -1), glm::vec3(1, 1, -1), glm::vec3(1, 1, 1) , Material(glm::vec4(1, 1, 1, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addQuad(glm::vec3(1, -1, 1), glm::vec3(1, 1, 1), glm::vec3(1, 1, -1), glm::vec3(1, -1, -1), Material(glm::vec4(1, 0.5, 0.5, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addQuad(glm::vec3(1, -1, -1), glm::vec3(1, 1, -1), glm::vec3(-1, 1, -1), glm::vec3(-1, -1, -1) , Material(glm::vec4(1, 1, 1, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addQuad(glm::vec3(-1, -1, -1), glm::vec3(-1, 1, -1), glm::vec3(-1, 1, 1), glm::vec3(-1, -1, 1), Material(glm::vec4(0.5, 0.5, 1, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addQuad(glm::vec3(-1, -1, 1), glm::vec3(-1, 1, 1), glm::vec3(1, 1, 1), glm::vec3(1, -1, 1), Material(glm::vec4(1, 1, 1, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addCube(glm::vec3(0, 1, 0), glm::vec3(0.5, 0.01, 0.5), glm::quat(glm::vec3(0, 0, 0)), Material(glm::vec4(0), glm::vec4(1, 1, 1, 10), glm::vec4(0)), triangles);

	addCube(glm::vec3(-0.4, -0.8, -0.3), glm::vec3(0.4), glm::vec3(0, 1, 0), Material(glm::vec4(0.4, 1, 0.4, 0), glm::vec4(0), glm::vec4(0)), triangles);
	addCube(glm::vec3(0.6, -0.65, -0.5), glm::vec3(0.4, 0.7, 0.4), glm::vec3(0, -0.7, 0), Material(glm::vec4(0.2, 0.6, 0.8, 0), glm::vec4(0), glm::vec4(0)), triangles);



	BufferGL rngState;
	std::vector<uint32_t> state;
	std::mt19937 generator(std::random_device{}());
	std::uniform_real_distribution<float> distribution(0, 1);
	for (int i = 0; i < wSize.x * wSize.y; i++) {
		state.push_back(distribution(generator) * (UINT32_MAX - 1));
	}
	rngState.allocateStorage(state.data(), state.size() * sizeof(uint32_t));
	rngState.bindToIndex(0, IndexType::SHADER_STORAGE);

	BufferGL sphereBuffer;
	sphereBuffer.allocateStorage(spheres.data(), spheres.size() * sizeof(Sphere));
	sphereBuffer.bindToIndex(1, IndexType::SHADER_STORAGE);

	BufferGL triangleBuffer;
	triangleBuffer.allocateStorage(triangles.data(), triangles.size() * sizeof(Triangle));
	triangleBuffer.bindToIndex(2, IndexType::SHADER_STORAGE);

	ShaderGL shaderDisplay, shaderTraceRay;
	shaderDisplay.addShaderStage("shaderDisplay.vert", ShaderStage::VERTEX);
	shaderDisplay.addShaderStage("shaderDisplay.frag", ShaderStage::FRAGMENT);
	shaderDisplay.createProgram();
	shaderTraceRay.addShaderStage("shaderTraceRay.comp", ShaderStage::COMPUTE);
	shaderTraceRay.createProgram();



	Camera camera;
	camera.position = glm::vec3(0, 0, 3);
	camera.fov = 70;
	camera.focalLength = 1;

	float sinPitch = std::sin(glm::radians(camera.pitch));
	float cosPitch = std::cos(glm::radians(camera.pitch));

	float sinYaw = std::sin(glm::radians(camera.yaw));
	float cosYaw = std::cos(glm::radians(camera.yaw));

	camera.front = glm::vec3(cosPitch * cosYaw, sinPitch, cosPitch * sinYaw);
	camera.right = glm::normalize(glm::cross(camera.front, glm::vec3(0, 1, 0)));
	camera.up = glm::normalize(glm::cross(camera.right, camera.front));







	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture.getID());

	bool fixed = true;
	int framesStill = 0;

	bool isFirstFrame = true;

	MultiTimer timer(100);
	while (!glfwWindowShouldClose(window.getWindow())) {

		mouse.update();
		keyboard.update();
		timer.update();

		if (isFirstFrame) {
			isFirstFrame = false;
		}

		framesStill++;

		float frametime = timer.getAverageInterval() / 1000;
		window.setTitle(std::to_string(frametime * 1000) + "," + std::to_string(framesStill));

		glfwPollEvents();

		glClear(GL_COLOR_BUFFER_BIT);

		shaderTraceRay.use();
		shaderTraceRay.setVector2i("wSize", wSize);
		shaderTraceRay.setVector3f("cameraPosition", camera.position);
		shaderTraceRay.setVector3f("cameraFront", camera.front);
		shaderTraceRay.setVector3f("cameraUp", camera.up);
		shaderTraceRay.setVector3f("cameraRight", camera.right);
		shaderTraceRay.setFloat("fov", camera.fov);
		shaderTraceRay.setFloat("focalLength", camera.focalLength);
		shaderTraceRay.setInt("sphereCount", spheres.size());
		shaderTraceRay.setInt("triangleCount", triangles.size());
		shaderTraceRay.setInt("samples", 1);
		shaderTraceRay.setInt("bounces", fixed ? 10 : 5);
		shaderTraceRay.setInt("framesStill", framesStill);
		 
		glBindImageTexture(0, texture.getID(), 0, GL_FALSE, 0, GL_READ_WRITE, GL_RGBA32F);
		glDispatchCompute(wSize.x / 16 + (wSize.x % 16 > 0), wSize.y / 16 + (wSize.y % 16 > 0), 1);
		glMemoryBarrier(GL_ALL_BARRIER_BITS);

		shaderDisplay.use();
		shaderDisplay.setInt("framesStill", framesStill);
		empty.use();
		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

		glfwSwapBuffers(window.getWindow());

		if (!fixed) {
			glm::vec2 delta = mouse.deltaPosition();
			if (delta != glm::vec2(0)) {

				camera.yaw += delta.x * frametime * 50 * camera.fov / 90;
				camera.pitch -= delta.y * frametime * 50 * camera.fov / 90;

				if (camera.pitch > 89.0f) {
					camera.pitch = 89.0f;
				}
				if (camera.pitch < -89.0f) {
					camera.pitch = -89.0f;
				}

				float sinPitch = std::sin(glm::radians(camera.pitch));
				float cosPitch = std::cos(glm::radians(camera.pitch));

				float sinYaw = std::sin(glm::radians(camera.yaw));
				float cosYaw = std::cos(glm::radians(camera.yaw));

				camera.front = glm::vec3(cosPitch * cosYaw, sinPitch, cosPitch * sinYaw);
				camera.right = glm::normalize(glm::cross(camera.front, glm::vec3(0, 1, 0)));
				camera.up = glm::normalize(glm::cross(camera.right, camera.front));

				glm::mat4 rotationMatrix = glm::mat4_cast(glm::quat(-glm::vec3(delta.y, delta.x, 0) * frametime));

				camera.front = rotationMatrix * glm::vec4(camera.front, 1);
				camera.up = rotationMatrix * glm::vec4(camera.up, 1);
				camera.right = rotationMatrix * glm::vec4(camera.right, 1);

				framesStill = 0;
			}

			float speed = frametime;
			if (keyboard.held(Key::LEFT_CONTROL)) {
				speed *= 5;
			}

			if (keyboard.held(Key::A)) { camera.position -= camera.right * speed; framesStill = 0; }
			if (keyboard.held(Key::D)) { camera.position += camera.right * speed; framesStill = 0; }
			if (keyboard.held(Key::W)) { camera.position += camera.front * speed; framesStill = 0; }
			if (keyboard.held(Key::S)) { camera.position -= camera.front * speed; framesStill = 0; }
			if (keyboard.held(Key::LEFT_SHIFT)) { camera.position -= glm::vec3(0, 1, 0) * speed; framesStill = 0; }
			if (keyboard.held(Key::SPACE)) { camera.position += glm::vec3(0, 1, 0) * speed; framesStill = 0; }
		}
		
		if (fixed && framesStill % 1000 == 0) {
			std::vector<glm::vec4> pixels(wSize.x * wSize.y);
			glGetTextureImage(texture.getID(), 0, GL_RGBA, GL_FLOAT, wSize.x * wSize.y * sizeof(glm::vec4), pixels.data());
			std::vector<glm::u8vec4> pixelsToSave(wSize.x * wSize.y);
			for (int i = 0; i < pixels.size(); i++) {

				glm::vec4 pixel = pixels[i] / glm::vec4(glm::vec3(framesStill), 1);
				pixel.x = std::clamp(pixel.x, 0.0f, 1.0f);
				pixel.y = std::clamp(pixel.y, 0.0f, 1.0f);
				pixel.z = std::clamp(pixel.z, 0.0f, 1.0f);

				pixelsToSave[i] = pixel * 255.0f;
			}
			std::string filename = "Images/RT_" + std::to_string(wSize.x) + "x" + std::to_string(wSize.y) + "_" + std::to_string(framesStill) + ".bmp";
			stbi_flip_vertically_on_write(true);
			stbi_write_bmp(filename.data(), wSize.x, wSize.y, 4, pixelsToSave.data());
		}
		
		if (keyboard.pressed(Key::TAB)) {
			fixed = !fixed;
			if (fixed) {
				window.setCursorMode(CursorMode::NORMAL);
			}
			else {
				window.setCursorMode(CursorMode::DISABLED);
			}
		}

		if (keyboard.held(Key::LEFT_CONTROL) && keyboard.pressed(Key::Q)) { glfwSetWindowShouldClose(window.getWindow(), true); }
	}

	return 0;
}